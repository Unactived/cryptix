# Cryptix
A tool to easily encrypt and decrypt text with your favorite cyphers

![](pub/morse.png)

A quick reminder is available for each cypher so you know what you're dealing with.

> Are there only traditional cyphers ?

Not necessarily, in the future there might be anything I find useful, like hashing or some steganography.
